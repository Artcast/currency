# Currency

This is a test using angular 5 and sass to get currencies and convert them.

### Prerequisites

Make sure you have Node version >= 8.0 and (NPM >= 5 or Yarn )

### Installing

Download repo and just follow commands

Install node modules
```
npm install
```

Run server

```
ng serve
```