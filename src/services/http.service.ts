import { Injectable } from "@angular/core";
import { Http, Response, Headers} from "@angular/http";
import { Observable } from "rxjs";
import 'rxjs/Rx';
@Injectable()
//Service to make request in api
export class HttpServices{
    public url = 'https://data.fixer.io/api/latest?';
    private accessKey = '3d71e2df828bce68ad2ffec58232b272';

    constructor(private http: Http){
    }

    getCurrency(base:string='USD',symbols:string='EUR'):Observable<any>{
        return this.http.get(this.url +'&base='+base+'&symbols='+symbols+'&access_key='+this.accessKey)
        .map(
            (response: Response) => {
                return response.json();
            }
        );
    }
}