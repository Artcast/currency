import { RatesModel } from "./rates.model";
//Model for response
export class ResponseModel {
    public success: boolean;
    public timestamp: number;
    public base:string;
    public date:string;
    public rates:any;
  
    constructor(success: boolean,timestamp: number,base:string,date:string,rates)
    {
        this.success = success;
        this.timestamp = timestamp;
        this.base = base;
        this.date = date;
        this.rates = rates;
    }
}