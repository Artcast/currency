import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpServices } from '../../services/http.service';
import { RatesModel } from '../../model/rates.model';
import { ResponseModel } from '../../model/response.model';
import { CurrencyPipe } from '@angular/common';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild('firstValue') firstValue: ElementRef;
  @ViewChild('secondValue') secondValue: ElementRef;
  //Currency which s going to be converted
  public currentCurrencyConvert:RatesModel = new RatesModel();
  //Currency base, in this case is USD
  public currentCurrencyBase:RatesModel = new RatesModel();
  //To config the minutes to do request, default 10 minutes
  private mins=10;
  //To format currency in blur event
  private formattedAmount: string = '0';
  //Construct to initialize variables
  constructor(private _service:HttpServices,private currencyPipe: CurrencyPipe) {
    //Inicialize base currency 
    this.currentCurrencyBase.name = 'USD';
    this.currentCurrencyBase.value = 1.0;
  }

  ngOnInit(){
    //First request to have data
    this.requestCurrency();
    //Interval to do request each 10 minutes
    setInterval(function()
    {
      this.requestCurrency();
    }, 1000*60*this.mins);    
  }

  //Request to get currencies, default USD
  private requestCurrency(){
    this._service.getCurrency().subscribe(
      (response: ResponseModel) => {
        
        this.currentCurrencyConvert.name = Object.keys(response.rates)[0];   
        this.currentCurrencyConvert.value = response.rates[Object.keys(response.rates)[0]];
    })
  }

  //On click calculate the value to convert
  public calculate()
  {
    var newVal = parseFloat(this.formattedAmount.replace(/[$,]/g, ""));
    var cal = newVal*this.currentCurrencyConvert.value;
    this.secondValue.nativeElement.value=this.currencyPipe.transform(cal,this.currentCurrencyConvert.name, true, '1.4-4');
  }

  //Transform amount in blur
  transformAmount(){
    try {
      if (typeof(this.firstValue.nativeElement.value) !== 'number')
        this.formattedAmount = this.currencyPipe.transform(this.firstValue.nativeElement.value,this.currentCurrencyBase.name, true, '1.4-4');
        this.firstValue.nativeElement.value = this.formattedAmount;
    } catch (e) {
      console.log(e);
    }
  }
  
  //Transform amount in focus
  removeCurrencyPipeFormat(){
    try {
      if (this.firstValue.nativeElement.value.indexOf('$') !== -1) {
        this.formattedAmount = this.formattedAmount.replace(/[$,]/g, "");
        this.firstValue.nativeElement.value = this.formattedAmount;
      } else {
        this.formattedAmount = '0';
      }
    } catch (e) {
      console.log(e);
    }
  }
}
